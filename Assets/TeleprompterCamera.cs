using System.Collections;
using System.Collections.Generic;
using TMPro;
using System;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Text;

public class TeleprompterCamera : MonoBehaviour
{
    #region Singleton
    private static TeleprompterCamera _instance;
    public static TeleprompterCamera Instance => _instance ? _instance : _instance = FindObjectOfType<TeleprompterCamera>();
    #endregion

    public event Action OnFail;
    public event Action OnWord;

    enum Movement {
        Normal,
        Scrollback,
        Scrollforward
    }

    public Camera screenshotCamera;
    public TextMeshPro textMesh;

    // These are defined by the text object.
    float textLineHeight = 24.0f;

    float cameraLowerBound = -23;
    float cameraUpperBound = 1;

    // [SerializeField] private float _scrollSpeed = 1f;

    float cameraLineHeight = 2.8f;

    // Game mechanic variables
    int currentLetterIndex = 0;

    Color32 _textUndecidedColor = new Color32(255, 255, 255, 255);
    Color32 _textCorrectColor = new Color32(0, 255, 0, 255);
    Color32 _textIncorrectColor = new Color32(255, 0, 0, 255);

    Movement moveState = Movement.Normal;
    private List<Color> _soFar = new List<Color>();
    private string _startText;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log($"lower bound {cameraLowerBound}");
        textLineHeight = (textMesh.textInfo.lineInfo.Length > 0
            ? textMesh.textInfo.lineInfo[0].lineHeight
            : 0.0f);

        _startText = textMesh.text;
        Keyboard.current.onTextInput += InputUpdate;
    }

    private int _ln;
    int GetLineNumber(int characterIndex) {
        int lineNumber = _ln;
        for (int li = _ln; li < textMesh.textInfo.lineInfo.Length; li++) {
            if (characterIndex >= textMesh.textInfo.lineInfo[li].firstVisibleCharacterIndex
                && characterIndex <= textMesh.textInfo.lineInfo[li].lastVisibleCharacterIndex + 1)
            {
                    lineNumber = li;
                    break;
            }
        }
        _ln = lineNumber;
        return lineNumber;
    }

    // Update is called once per frame
    void Update()
    {
        float linePos = cameraUpperBound - ((float)GetLineNumber(currentLetterIndex)) * cameraLineHeight;
        screenshotCamera.transform.position = new Vector3(screenshotCamera.transform.position.x, linePos, screenshotCamera.transform.position.z);
        /*
         * float cameraPosition = screenshotCamera.transform.position.y;
        float linePosition = cameraPosition + ((float) GetLineNumber(currentLetterIndex)) * cameraLineHeight;
        // Movement
        if (moveState == Movement.Normal) {
            screenshotCamera.transform.Translate(0, -0.01f, 0);
            if (linePosition > cameraLowerBound) {
                Debug.Log("To scrolling forward from normal!");
                moveState = Movement.Scrollforward;
            }
            if (linePosition < cameraUpperBound) {
                Debug.Log("To scrollback from normal!");
                moveState = Movement.Scrollback;
            }
        } else if (moveState == Movement.Scrollback) {
            screenshotCamera.transform.Translate(0, 0.1f, 0);
            if (linePosition > 0) {
                Debug.Log("Back to normal from scrolling back!");
                moveState = Movement.Normal;
            }
        } else if (moveState == Movement.Scrollforward) {
            screenshotCamera.transform.Translate(0, -0.1f, 0);
            Debug.Log("cameraLineHeight: " + cameraLineHeight);
            Debug.Log("CameraLowerBound: " + cameraLowerBound);
            if (linePosition + cameraLineHeight < cameraLowerBound) {
                Debug.Log("Back to normal from scrolling forward!");
                moveState = Movement.Normal;
            }
        }
        */
        textMesh.UpdateVertexData(TMP_VertexDataUpdateFlags.All);
    }

    public struct Word
    {
        public int StartIdx;
        public int EndIdx;

        public int Length => EndIdx - StartIdx;

        public Word(int start, int end)
        {
            StartIdx = start;
            EndIdx = end;
        }

        public override string ToString()
        {
            return Instance._startText.Substring(StartIdx, Length);
        }
    }

    public IEnumerable<Word> WordIterator()
    {
        int start = currentLetterIndex;
        int last = textMesh.textInfo.lineInfo[_ln + 1].lastCharacterIndex;
        while (_startText[start] != ' ' && start < last) start++;
        start++;

        for (int i = start; i <= last; i++)
        {
            if (_startText[i] == ' ')
            {
                yield return new Word(start, i);
                start = ++i;
            }
        }
        yield return new Word (start, last);
    }

    public void ReverseNextWordOnLine()
    {
        var lineInfo = textMesh.textInfo.lineInfo[_ln];
        var iter = WordIterator().GetEnumerator();

        Debug.Log("reversing word");
        if (iter.MoveNext())
        {
            Word w = iter.Current;
            Debug.Log($"reversing {w}");
            var builder = new StringBuilder(textMesh.text);
            builder.Remove(w.StartIdx, w.Length);
            char[] charArray = w.ToString().ToCharArray();
            Array.Reverse(charArray);
            var reversed = new string(charArray);
            Debug.Log($"'{w}' became '{reversed}'");
            builder.Insert(w.StartIdx, reversed);
            UpdateText(builder.ToString());
            for(int i = w.StartIdx; i < w.EndIdx; i++)
            {
                TMP_CharacterInfo currentCharInfo = textMesh.textInfo.characterInfo[i];
                int meshIndex = currentCharInfo.materialReferenceIndex;
                int vertexIndex = currentCharInfo.vertexIndex;
                Color32[] vertexColors = textMesh.textInfo.meshInfo[meshIndex].colors32;

                vertexColors[vertexIndex + 0] = Color.yellow;
                vertexColors[vertexIndex + 1] = Color.yellow;
                vertexColors[vertexIndex + 2] = Color.yellow;
                vertexColors[vertexIndex + 3] = Color.yellow;

            }
        }
    }

    public void UpdateText(string text)
    {
        textMesh.text = text;
        for (int i = 0; i <= currentLetterIndex; i++)
        {
            TMP_CharacterInfo currentCharInfo = textMesh.textInfo.characterInfo[i];
            int meshIndex = currentCharInfo.materialReferenceIndex;
            int vertexIndex = currentCharInfo.vertexIndex;
            // Just copied this shit.
            Color32[] vertexColors = textMesh.textInfo.meshInfo[meshIndex].colors32;
            vertexColors[vertexIndex + 0] = _soFar[i];
            vertexColors[vertexIndex + 1] = _soFar[i];
            vertexColors[vertexIndex + 2] = _soFar[i];
            vertexColors[vertexIndex + 3] = _soFar[i];
        }
    }

    public void InputUpdate(char c)
    {
        TMP_CharacterInfo currentCharInfo = textMesh.textInfo.characterInfo[currentLetterIndex];
        int meshIndex = currentCharInfo.materialReferenceIndex;
        int vertexIndex = currentCharInfo.vertexIndex;
        // Just copied this shit.
        Color32[] vertexColors = textMesh.textInfo.meshInfo[meshIndex].colors32;
        if (c != '\b' && c != '\n')
        {
            int lineNumber = GetLineNumber(currentLetterIndex);
            if (currentLetterIndex < textMesh.text.Length)
            {
                if (_startText[currentLetterIndex] == c)
                {
                    vertexColors[vertexIndex + 0] = _textCorrectColor;
                    vertexColors[vertexIndex + 1] = _textCorrectColor;
                    vertexColors[vertexIndex + 2] = _textCorrectColor;
                    vertexColors[vertexIndex + 3] = _textCorrectColor;
                    _soFar.Add(_textCorrectColor);

                    if (currentLetterIndex != 0 && _startText[currentLetterIndex - 1] != ' ' && c == ' ')
                    {
                        OnWord?.Invoke();
                    }
                }
                else
                {
                    vertexColors[vertexIndex + 0] = _textIncorrectColor;
                    vertexColors[vertexIndex + 1] = _textIncorrectColor;
                    vertexColors[vertexIndex + 2] = _textIncorrectColor;
                    vertexColors[vertexIndex + 3] = _textIncorrectColor;
                    _soFar.Add(_textIncorrectColor);
                    OnFail?.Invoke();
                }
                
                if (currentLetterIndex < _startText.Length - 1)
                {
                    currentLetterIndex++;
                }
            }
        }
    }
}
