using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FaceManager : MonoBehaviour
{ 
    [SerializeField] private GameObject face1;
    [SerializeField] private GameObject face2;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float rageVal = AngerMeter.Instance.Meter.Value / AngerMeter.Instance.Meter.MaxValue;

        if (rageVal > 0.5f)
        {
            face1.SetActive(false);
            face2.SetActive(true);
        }
        else
        {
            face2.SetActive(false);
            face1.SetActive(true);
        }
    }
}
