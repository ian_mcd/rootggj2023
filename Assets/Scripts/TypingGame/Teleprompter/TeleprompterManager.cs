using UnityEngine;

public class TeleprompterManager : MonoBehaviour
{
    [SerializeField]
    int _numTeleprompterLines = 6;
    [SerializeField]
    float _teleprompterRate = 2f;

    RotatingBoundedQueue<Line> _teleprompterLines;
    Line _activeLine => _teleprompterLines.GetValue(0);

    public Line[] TeleprompterLines => _teleprompterLines.GetArray();

    void Awake()
    {
        _teleprompterLines = new RotatingBoundedQueue<Line>(_numTeleprompterLines);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Shift lines up on the screen
        float deltaY = Time.deltaTime * _teleprompterRate;

        // if a line goes out of view, destroy it and have side effects
        // spawn a new line at the end
        // TODO: determine if line is out of view (collider?)
        UpdateTeleprompter();
    }

    public void TypeCharacter(char c)
    {
        bool correctChar = _activeLine.TypeCharacter(c);
        if (!correctChar)
        {
            // side effect?
        }
        if (_activeLine.Complete)
        {
            UpdateTeleprompter();
            // perform positive side effect?
        }
    }

    void UpdateTeleprompter()
    {
        Line removedLine = _teleprompterLines.Add(GetNextLine());
        if (removedLine != null)
        {
            // perform side effect
        }
    }

    Line GetNextLine()
    {
        // TODO: Get next words from script that fit w/in Line.MaxCharLength
        return new Line("temp");
    }
}
