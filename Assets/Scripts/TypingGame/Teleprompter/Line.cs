using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

// created word class because we want a Text object per word so we can mess up each word.
public class Word
{
    public string Value { get; private set; }

    public Word(string word)
    {
        Value = word;
    }
}

public class Line
{
    public readonly static int MaxCharLength = 20;

    private List<Word> _words;
    public List<Word> Words => _words;

    public override string ToString()
    {
        return string.Join(", ", Words);
    }

    int _curCharPos = 0;

    public bool Complete => _curCharPos >= ToString().Length;

    public Line (string line)
    {
        _words = new List<Word>();
        foreach (string word in line.Split(' '))
        {
            _words.Add(new Word(word));
        }
    }

    /*
     * @param c: character to be typed
     * @return bool: valid character to type
     */
    public bool TypeCharacter(char c)
    {
        bool correctChar = _curCharPos > ToString().Length && c == ToString()[_curCharPos];
        if (correctChar)
        {
            _curCharPos++;
        }
        return correctChar;
    }
}
