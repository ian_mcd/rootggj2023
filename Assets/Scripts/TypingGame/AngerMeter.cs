using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AngerMeter : MonoBehaviour
{
    #region Singleton
    private static AngerMeter _instance;
    public static AngerMeter Instance => _instance ? _instance : _instance = FindObjectOfType<AngerMeter>();
    #endregion

    public Meter Meter;

    [SerializeField] Color _chill = Color.blue;
    [SerializeField] Color _angry = Color.red;
    [SerializeField] private float _meterDecayInterval = 1f;
    [SerializeField] private float _meterDecayValue = 1f;
    [SerializeField] private float _mistakeValue = 3f;
    [SerializeField] private float _finishWordValue = 3f;

    private Image _bar;

    private void Start()
    {
        _bar = Meter.GetComponent<Image>();
        Meter.OnMeterChange += (value) => _bar.color = Color.Lerp(_chill, _angry, value);
        TeleprompterCamera.Instance.OnFail += () => { Meter.Value += _mistakeValue; };
        TeleprompterCamera.Instance.OnWord += () => { Debug.Log("reducing meter");  Meter.Value -= _finishWordValue; };

        StartCoroutine(MeterDecay());
    }



    IEnumerator MeterDecay()
    {
        while (true)
        {
            yield return new WaitForSeconds(_meterDecayInterval);
            Meter.Value += _meterDecayValue;
        }
    }
}
