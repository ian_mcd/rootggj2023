using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

// Set a value from 0 - 1 to show percentage of meter filled
[RequireComponent(typeof(RectTransform))]
public class Meter : MonoBehaviour
{

    public event Action<float> OnMeterChange;

    private RectTransform _rt;
    public RectTransform RectTransform => _rt;

    [SerializeField] private float _maxValue = 20;
    [SerializeField] float _startValue = 20;
    [SerializeField] private bool _vertical = true;

    private Vector2 _modifier => _vertical ? Vector2.up : Vector2.right;

    private float _value;
    private Vector2 _startSize;

    public float Value
    {
        get { return _value; }
        set
        {
            SetValue(value);
        }
    }
    
    public float MaxValue
    {
        get { return _maxValue; }
        set
        {
            SetValue(_maxValue);
        }
    }

    public void Awake()
    {
        _rt = GetComponent<RectTransform>();
    }

    private void Start()
    {
        var mod = (new Vector2(1, 1) - _modifier);
        _rt.pivot = new Vector2(_rt.pivot.x * mod.x, _rt.pivot.y * mod.y);
        _startSize = _rt.rect.size;
        SetValue(2);
    }

    // TODO: implement
    public void SetValue(float value)
    {
        _value = Mathf.Clamp(value, 0, _maxValue);
        var mod = 1 - value / _maxValue;
        _rt.sizeDelta = - _modifier * mod * Vector2.Dot(_startSize, _modifier);
        OnMeterChange?.Invoke(value / _maxValue);
    }
}
