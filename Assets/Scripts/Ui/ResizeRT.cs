using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RectTransform))]
[RequireComponent(typeof(RawImage))]
public class ResizeRT : MonoBehaviour
{
    private RectTransform _rt;
    private RawImage _ri;
    private Rect _prevBounds;
    [SerializeField] private Camera _camera;
    private void Awake()
    {
        _rt = GetComponent<RectTransform>();
        _ri = GetComponent<RawImage>();
    }

    private void UpdateRt()
    {
        _prevBounds = _rt.rect;
        Debug.Log(_prevBounds);
        var rt = new RenderTexture(new RenderTextureDescriptor((int)_rt.rect.width, (int)_rt.rect.height));
        _camera.targetTexture = rt;
        _ri.texture = rt;

    }
    private void Update()
    {
        if (_rt.rect != _prevBounds) UpdateRt();
    }
}
