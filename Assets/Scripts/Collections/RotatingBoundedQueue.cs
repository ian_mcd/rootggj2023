using System.Collections.Generic;

public class RotatingBoundedQueue<T>
{
    int _firstElementInd;
    int _lastElementInd;
    int _numItems;
    T[] _queue;

    public bool IsFull => _numItems == _queue.Length;
    public int Count => _queue.Length;

    public RotatingBoundedQueue(int capacity)
    {
        _queue = new T[capacity];
        _firstElementInd = 0;
        _lastElementInd = -1;
        _numItems = 0;
    }

    public T GetValue(int index)
    {
        return _queue[_firstElementInd + index % _queue.Length];
    }

    public T Add(T item)
    {
        bool isFullBeforeAdd = IsFull;
        T firstElement = _queue[_firstElementInd];
        _lastElementInd = _lastElementInd + 1 % _queue.Length ;
        _queue[_lastElementInd] = item;
        if (isFullBeforeAdd)
        {
            _firstElementInd = _firstElementInd + 1 % _queue.Length;
            return firstElement;
        }
        return default(T);
    }

    public T[] GetArray()
    {
        T[] cleanArray = new T[_queue.Length];
        for (int i = 0; i < _queue.Length; i++)
        {
            cleanArray[i] = _queue[_firstElementInd + i % _queue.Length];
        }
        return cleanArray;
    }
}
