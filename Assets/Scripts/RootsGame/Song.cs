using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public enum NoteValue
{
    Left,
    Up,
    Down,
    Right
}


[Serializable]
public struct NoteUnit
{
    public NoteValue Value;
    public float Time;
    public float EndTime;

    public NoteUnit(NoteValue value, float time, float endtime)
    {
        Value = value;
        Time = time;
        EndTime = endtime;
    }

    public NoteUnit(NoteValue value, float time) : this(value, time, -1) { }

    public override string ToString()
    {
        return $"t: {Time} -> {Value}";
    }
}

[Serializable]
public class Song
{
    public NoteUnit[] Notes;

    public Song() { }

    public Song(IEnumerable<NoteUnit> notes) : this(new List<NoteUnit>(notes).ToArray())
    { 
    }

    public Song(NoteUnit[] notes)
    {
        Notes = notes;
    }
}
