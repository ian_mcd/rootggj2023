using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "song config", menuName = "roots/song settings")]
public class SongConfig : ScriptableObject
{
    public float ApproachRate;
    public AudioClip SongClip;
    public AudioClip FailClip;
    public float Time => SongClip.length;

    public TextAsset SongText;

    public Song ToSong()
    {
        return new SongLoader(SongText.text).Load();
    }
}