using RotaryHeart.Lib.SerializableDictionary;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.InputSystem;

public partial class SongManager : MonoBehaviour
{
    #region serializableDict
    [Serializable]
    public class NoteLocationDictionary : SerializableDictionaryBase<NoteValue, Transform> { }
    [Serializable]
    public class ActionNoteDictionary : SerializableDictionaryBase<NoteValue, InputActionReference> { }

    [Serializable]
    public class NotePrefabsDictionary : SerializableDictionaryBase<NoteValue, Note> { }
    #endregion
    #region Singleton
    private static SongManager _instance;
    public static SongManager Instance => _instance ? _instance : _instance = FindObjectOfType<SongManager>();
    #endregion



    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private SongConfig _config;

    public PlayerInput pi;
    [SerializeField] private ActionNoteDictionary _actions;
    [SerializeField] private NotePrefabsDictionary _notePrefabs;
    [SerializeField] private NoteLocationDictionary _noteSpawn;
    [SerializeField] private NoteLocationDictionary _noteHit;
    [SerializeField] private NoteLocationDictionary _noteDestroy;

    private Song _song;
    private bool _songStarted = false;
    public bool Started => _songStarted;
    private float _audioStartTime;

    private int _incomingNoteIdx = 0;
    private int _prevNoteIdx = 0;

    private bool _lastFail = false;
    public bool LastFail
    {
        get { return _lastFail; }
        set
        {
            Debug.Log("Setter");
            if (value != _lastFail)
            {
                Debug.Log("clip swap");
                _lastFail = value;
                if (_config.FailClip == null) return;
                AudioClip c = value  ? _config.FailClip : _config.SongClip;
                float time = SongTime;
                _audioSource.clip = c;
                _audioSource.time = time;
                _audioSource.Play();
            }
        }

    }

    public float SongTime => (float)AudioSettings.dspTime - _audioStartTime;
    public NoteUnit LastNote => _song.Notes[_prevNoteIdx];
    public NoteUnit? NextNote => (_incomingNoteIdx == _song.Notes.Length) ? null : _song.Notes[_incomingNoteIdx];

    [SerializeField] float _hitRadius = 1;
    public float HitRadius => _hitRadius;


    private Dictionary<NoteValue, Queue<Note>> _lanes = new Dictionary<NoteValue, Queue<Note>>();

    public void StartSong()
    {
        // initialize Lanes
        foreach (NoteValue noteV in Enum.GetValues(typeof(NoteValue)))
        {
            _lanes[noteV] = new Queue<Note>();
        }
        _audioStartTime = (float) AudioSettings.dspTime;
        _songStarted = true;
        LastFail = true;
        _audioSource.clip = _config.SongClip;
        _audioSource.Play();
        _incomingNoteIdx = 0;
        Debug.Log("Song Started");
    }

    private void SpawnNote(NoteUnit note)
    {
        Note n = Instantiate(_notePrefabs[note.Value], SpawnPosition(note), SpawnRotation(note));
        n.Init(note);
        _lanes[note.Value].Enqueue(n);
        _prevNoteIdx = _incomingNoteIdx++;
    }

    public void DestroyNote(Note n)
    {
        Destroy(n.gameObject);
    }

    public void HitLane(NoteValue noteType)
    {
        var lane = _lanes[noteType];
        Note n;
        // evict the deleting notes from queue  that will be removed l8r
        while (
            lane.TryPeek(out n)
            && (
                n == null ||
                SongTime > n.NoteUnit.Time + _hitRadius / NoteSpeed
            )
        ) lane.Dequeue();

        if (lane.TryPeek(out n)) Debug.Log($"n is {n}");
        if (lane.TryPeek(out n) && Vector3.Distance(n.transform.position, HitPosition(n.NoteUnit)) > _hitRadius)
        {
            NoNoteEffect();
            return;
        }
        if (n)
        {
            // TODO: handle scoring
            float score = ScoreHit(n);
            RootMeter.Instance.Meter.Value += score;
            if (lane.Dequeue() != n) Debug.LogWarning("WTF");
            DestroyNote(n);
        }
    }

    // TODO: Come up with a better scorring system
    private float ScoreHit(Note n)
    {
        LastFail = false;
        return (_hitRadius - n.HitDistance) / _hitRadius;
    }

    // TODO: implement behavior when there is no note to hit at the moment
    private void NoNoteEffect()
    {
        RootMeter.Instance.Meter.Value -= 1f;
        LastFail = true;
    }

    private void UpdateSong()
    {
        if (NextNote != null && SongTime >= GetSpawnTime(NextNote.Value)) SpawnNote(NextNote.Value);
        foreach (NoteValue nv in _actions.Keys)
        {
            if (_actions[nv].action.triggered)
            {
                Debug.Log($"hit {nv}");
                HitLane(nv);
            }
        }

        if (SongTime >= _audioSource.clip.length)
        {
            // Loop the song
            _audioSource.time = 0;
            StartSong();
        }
    }

    public void Start()
    {
        // Make sure Song is loaded
        _song = _config.ToSong();
        _prevNoteIdx = _song.Notes.Length - 1;
        StartSong();

    }

    public void Update()
    {
        if (_songStarted) UpdateSong();
    }
}
