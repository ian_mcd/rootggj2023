using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class RootMeter : MonoBehaviour
{
    #region Singleton
    private static RootMeter _instance;
    public static RootMeter Instance => _instance ? _instance : _instance = FindObjectOfType<RootMeter>();
    #endregion

    [SerializeField] private Meter _meter;
    
    public Meter Meter => _meter;
}
