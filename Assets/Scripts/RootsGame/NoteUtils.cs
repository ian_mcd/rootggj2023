using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class SongManager : MonoBehaviour
{
    #region NoteUtils
    public float NoteSpeed => _config.ApproachRate;

    public Vector3 SpawnPosition(NoteUnit note)
    {
        return _noteSpawn[note.Value].position;
    }

    public Vector3 HitPosition(NoteUnit note)
    {
        return _noteHit[note.Value].position;
    }

    public Vector3 DestroyPosition(NoteUnit note)
    {
        return _noteDestroy[note.Value].position;
    }

    public Quaternion SpawnRotation(NoteUnit note)
    {
        return _noteSpawn[note.Value].rotation;
    }

    public Quaternion HitRotation(NoteUnit note)
    {
        return _noteHit[note.Value].rotation;
    }

    public Quaternion DestroyRotation(NoteUnit note)
    {
        return _noteDestroy[note.Value].rotation;
    }

    public float DistanceToHit(NoteUnit note)
    {
        return Vector3.Distance(SpawnPosition(note), HitPosition(note));
    }

    public float DistanceToDestroy(NoteUnit note)
    {
        return Vector3.Distance(DestroyPosition(note), HitPosition(note));
    }

    public float GetSpawnTime(NoteUnit note)
    {
        return note.Time - DistanceToHit(note) / NoteSpeed;
    }

    public float GetHitTime(NoteUnit note)
    {
        return note.Time;
    }

    public float GetDestroyTime(NoteUnit note)
    {
        return note.Time + DistanceToDestroy(note) / NoteSpeed;
    }
    #endregion
}
