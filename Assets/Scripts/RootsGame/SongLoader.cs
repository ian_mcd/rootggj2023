using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text;
using System;

public class SongLoader
{
    private string _raw;
    public SongLoader(string song)
    {
        _raw = song;
    }

    private int CompareNoteByTime(NoteUnit note1, NoteUnit note2)
    {
        return note1.Time.CompareTo(note2.Time);
    }

    public Song Load()
    {
        Song song = JsonUtility.FromJson<Song>(_raw);
        Array.Sort(
            song.Notes,
            CompareNoteByTime
        );
        return song;
    }
}
