using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : MonoBehaviour
{
    private NoteUnit _note;
    public NoteUnit NoteUnit => _note;

    private Vector3 _hitPos;
    public float HitDistance => Vector3.Distance(transform.position, _hitPos);

    public void Init(NoteUnit note)
    {
        _note = note;
        _hitPos = SongManager.Instance.HitPosition(_note);
        StartCoroutine(PlayNoteRoutine());

    }

    IEnumerator PlayNoteRoutine()
    {
        float tStart = SongManager.Instance.GetSpawnTime(_note);
        float tHit = SongManager.Instance.GetHitTime(_note);
        float tDestroy = SongManager.Instance.GetDestroyTime(_note);

        Vector3 spawnPos = SongManager.Instance.SpawnPosition(_note);
        Vector3 hitPos = _hitPos;
        Vector3 destroyPos = SongManager.Instance.DestroyPosition(_note);

        Quaternion spawnRot = SongManager.Instance.SpawnRotation(_note);
        Quaternion hitRot = SongManager.Instance.HitRotation(_note);
        Quaternion destroyRot = SongManager.Instance.DestroyRotation(_note);

        // early start case
        if (tStart < 0)
        {
            var val = Mathf.InverseLerp(tStart, tHit, 0);
            Debug.Log(val);
            spawnPos = Vector3.Lerp(spawnPos, hitPos, val);
            spawnRot = Quaternion.Slerp(spawnRot, hitRot, val);
            transform.position = spawnPos;
            tStart = 0;
        }

        while (SongManager.Instance.SongTime <= tHit)
        {
            var t = Mathf.InverseLerp(tStart, tHit, SongManager.Instance.SongTime);
            transform.position = Vector3.Lerp(spawnPos, hitPos, t);
            transform.rotation = Quaternion.Slerp(spawnRot, hitRot, t);
            yield return null;

        }
        while (SongManager.Instance.SongTime <= tDestroy)
        {
            var t = Mathf.InverseLerp(tHit, tDestroy, SongManager.Instance.SongTime);
            transform.position = Vector3.Lerp(hitPos, destroyPos, t);
            transform.rotation = Quaternion.Slerp(hitRot, destroyRot, t);
            yield return null;
        }
        yield return null;
        // SongManager.Instance.DestroyNote(this);
        Destroy(gameObject);
    }

}
