using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;


public class MurManager : MonoBehaviour
{
    #region Singleton
    private static MurManager _instance;
    public static MurManager Instance => _instance ? _instance : _instance = FindObjectOfType<MurManager>();
    #endregion

    [SerializeField] AudioClip[] _murs;
    [SerializeField] AudioSource _source;

    private Random _r;

    private void Start()
    {
        TeleprompterCamera.Instance.OnWord += Mur;
        _r = new Random((int)Time.time);
    }

    public void Mur()
    {
        _source.time = 0;
        _source.clip = _murs[_r.Next(_murs.Length -1)];
        _source.Play();
    }
}
