using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using RotaryHeart.Lib.SerializableDictionary;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class FuckManager : MonoBehaviour
{
    public enum Fuck
    {
        Reverse,
        Anger
    }

    #region serializableDict
    [Serializable]
    public class FuckActionDictionary : SerializableDictionaryBase<Fuck, InputActionReference> { }
    [Serializable]
    public class FuckCostDictionary : SerializableDictionaryBase<Fuck, float> { }
    #endregion
    #region Singleton
    private static FuckManager _instance;
    public static FuckManager Instance => _instance ? _instance : _instance = FindObjectOfType<FuckManager>();
    #endregion

    [SerializeField] private FuckActionDictionary _actions;
    [SerializeField] private FuckCostDictionary _costs;

    private Dictionary<Fuck, Action> _onFuck = new Dictionary<Fuck, Action>();

    private int _RageID;

    public void Start()
    {
        _onFuck[Fuck.Reverse] = TeleprompterCamera.Instance.ReverseNextWordOnLine;
        _onFuck[Fuck.Anger] = () => AngerMeter.Instance.Meter.Value += _costs[Fuck.Anger];
        _RageID = Shader.PropertyToID("Rage");
    }

    public void Update()
    {
        foreach (Fuck f in _actions.Keys)
        {
            if (_actions[f].action.triggered && RootMeter.Instance.Meter.Value >= _costs[f])
            {
                Debug.Log($"fuck {f}");
                if (_onFuck.ContainsKey(f))
                {
                    _onFuck[f].Invoke();
                    RootMeter.Instance.Meter.Value -= _costs[f];
                }
            }
        }

        if (AngerMeter.Instance.Meter.Value >= AngerMeter.Instance.Meter.MaxValue)
        {
            SceneManager.LoadScene("GameOver");
            Debug.Log("GG");
        }

        float rageVal = AngerMeter.Instance.Meter.Value / AngerMeter.Instance.Meter.MaxValue;
        Shader.SetGlobalFloat(_RageID, rageVal);
        
    }
}
