import json
import click
from pathlib import Path
import sys
import random

@click.command()
@click.option("--file", "-f", required=True)
@click.option("--num-notes", "-n", type=int, default=3)
@click.option("--to-close", "-t", type=float, default=.2)
def main(file: str, num_notes: int, to_close: float):
    p = Path(file)
    notes = []
    beat = set(range(num_notes))
    last_notes = []

    with p.open() as f:
        for line in f:
            time, endtime, _label = line.split()
            endtime = endtime if time != endtime else -1
            time = float(time)
            endtime = float(endtime)
        
            if len(last_notes) >= num_notes:
                while len(last_notes) > 0 and last_notes[0]['Time'] + to_close < time:
                    last_notes = last_notes[1:]
            available = beat.difference(n['Value'] for n in last_notes)
            if len(available) == 0: continue
            note = {
                "Value": random.randint(0, num_notes),
                "Time": time,
                "EndTime": endtime
            }
            last_notes.append(note)
            notes.append(note)

    print(json.dumps({
        "Notes": notes
    }, indent=4))

if __name__ == "__main__":
    main()